# jenkins-management

## On Windows
1) Open up a command prompt window and do the following:

2) Create a bridge network in Docker 
```
docker network create jenkins
```
3) Run a docker:dind Docker image
* Create these two folders C:/jenkins-data and C:/jenkins-docker-certs
```
docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume C:/jenkins-docker-certs:/certs/client --volume C:/jenkins-data:/var/jenkins_home docker:dind
```
4) Customise official Jenkins Docker image, by executing below two steps:
* Create Dockerfile with the following content:
```
FROM jenkins/jenkins:2.303.1-jdk11
USER root
RUN apt-get update && apt-get install -y apt-transport-https \
       ca-certificates curl gnupg2 \
       software-properties-common
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN apt-key fingerprint 0EBFCD88
RUN add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/debian \
       $(lsb_release -cs) stable"
RUN apt-get update && apt-get install -y docker-ce-cli
USER jenkins
RUN jenkins-plugin-cli --plugins "blueocean:1.24.7 docker-workflow:1.26"
```
* Build a new docker image from this Dockerfile and assign the image a meaningful name, e.g. "myjenkins-blueocean:1.1":
```
docker build -t myjenkins-blueocean:1.1 .
```
5) Run your own myjenkins-blueocean:1.1 image as a container in Docker using the following docker run command:
```
docker run --name jenkins-blueocean --rm --detach --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 --volume C:/jenkins-data:/var/jenkins_home --volume C:/jenkins-docker-certs:/certs/client:ro --publish 8080:8080 --publish 50000:50000 myjenkins-blueocean:1.1
```
## Unlocking Jenkins
1) When you first access a new Jenkins instance, you are asked to unlock it using an automatically-generated password.
```
docker exec ${CONTAINER_ID or CONTAINER_NAME} cat /var/jenkins_home/secrets/initialAdminPassword
```
2) Browse to http://localhost:8080 (or whichever port you configured for Jenkins when installing it) and wait until the Unlock Jenkins page appears.
3) On the Unlock Jenkins page, paste this password into the Administrator password field and click Continue.
 
## Install Suggested Plugins
After unlocking Jenkins, the Customize Jenkins page appears. Here you can install any number of useful plugins as part of your initial setup. Select: **Install suggested plugins**
  
## Creating the first administrator user
Finally, after customizing Jenkins with plugins, Jenkins asks you to create your first administrator user.

1) When the Create First Admin User page appears, specify the details for your administrator user in the respective fields and click Save and Finish.
2) When the Jenkins is ready page appears, click Start using Jenkins.
3) If required, log in to Jenkins with the credentials of the user you just created and you are ready to start using Jenkins!

# Other important info
Jenkins Website Instruction: https://www.jenkins.io/doc/book/installing/docker/

## Accessing the Docker container
This means you could access your docker container (through a separate terminal/command prompt window) with a docker exec command like:
```
docker exec -it jenkins-blueocean bash
```
## Accessing the Docker logs
Your <docker-container-name> can be obtained using the docker ps command
```
docker logs <docker-container-name>
```
## Accessing the Jenkins home directory
If you mapped the Jenkins home directory (/var/jenkins_home) to one on your machine’s local file system (i.e. in the docker run …​ command above), then you can access the contents of this directory through your machine’s usual terminal/command prompt. You can also access the same directory directly within the docker container using
```
docker container exec -it jenkins-blueocean bash
cd /var/jenkins_home
```
